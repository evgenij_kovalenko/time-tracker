<?php

/* @var $this \yii\web\View */

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Time tracker tool</title>
    <link rel="icon" type="image/x-icon" href="favicon.ico">
    <link rel="stylesheet" href="/css/site.css">
</head>
<body>
    <?= $content ?>

    <footer class="footer">
        <div class="container">
        </div>
    </footer>
</body>
</html>
