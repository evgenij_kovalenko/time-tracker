<?php

$config = [
    'id' => 'time-tracker-api',
    'name' => 'Time Tracker API',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '07qr6MrrxmhGFWAz-MqRVYpnhsmK2l1s',
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => false,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'enableStrictParsing' => true,
            'rules' => [
                ['class' => 'yii\rest\UrlRule', 'controller' => 'project'],
//                'api/project' => 'api/project/index',
//                'api/project/<id:\d+>' => 'api/project/view',
//                'POST api/project' => 'api/project/create',
//                'PUT,PATCH api/project/<id:\d+>' => 'api/project/update',
//                'DELETE api/project/<id:\d+>' => 'api/project/delete',
//
//                'api/task/<id:\d+>' => 'api/task/view',
//                'api/tasks/<project_id:\d+>' => 'api/task/index',
//                'POST api/task' => 'api/task/create',
//                'PUT,PATCH api/task/<id:\d+>' => 'api/task/update',
//                'DELETE api/task/<id:\d+>' => 'api/task/delete',
//                'POST api/task/<id:\d+>/<action:start|stop>' => 'api/task/<action>',
//
//                'api/time-intervals/<task_id:\d+>' => 'api/time-interval/index',
//                'POST api/time-interval' => 'api/time-interval/create',
//                'DELETE api/time-interval/<id:\d+>' => 'api/time-interval/delete',
//
//                'POST api/user' => 'api/user/register',
//                'DELETE api/user' => 'api/user/delete',
//                'POST api/user/<action:login|logout>' => 'api/user/<action>',
            ]
        ],
    ],
];

return $config;
