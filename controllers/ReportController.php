<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use app\models\DailyReport;
use app\models\AverageReport;

class ReportController extends Controller
{
	public function actionDailyReport()
	{
		$report = new DailyReport();

		return $this->render('daily', [
			'model' => $report->toArray(),
		]);
	}

	public function actionAverageReport()
	{
		$report = new AverageReport();

		return $this->render('average', [
			'model' => $report->toArray(),
		]);
	}
}
