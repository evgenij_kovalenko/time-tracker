<?php

namespace app\controllers;

use Yii;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

use app\models\Project;
use app\models\Task;
use app\models\TaskSearch;

/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    public $project;

    public function actionIndex()
    {
        return [];
    }

    /**
     * @param $id
     *
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        return $this->findModel($id);
    }

    /**
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Task();
        if ($this->project) {
            $model->project_id = $this->project->id;
        }

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/project/view', 'id' => $model->project_id]);
        } else {
            return $this->render('create', [
                'model' => $model,
                'project' => $this->project,
                'statusList' => Task::getStatusList(),
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['/project/view', 'id' => $model->project_id]);
        } else {
            return $this->render('update', [
                'model' => $model,
                'statusList' => Task::getStatusList(),
            ]);
        }
    }

    /**
     * @param $id
     *
     * @return \yii\web\Response
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
    public function actionDelete($id)
    {
        $model = $this->findModel($id);
        $projectId = $model->project_id;
        $model->delete();

        return $this->redirect(['/project/view', 'id' => $projectId]);
    }

    /**
     * Finds the Task model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Task the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Starts time tracking on selected task
     * @param integer $id
     * @return mixed
     */
    public function actionStart($id)
    {
        $task = $this->findModel($id);
        if (!$task->isActive() || $task->isRunning()) {
            return $this->goBack();
        }

        $task->run();

        return $this->redirect(['/project/view', 'id' => $task->project->id]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionStop($id)
    {
        $task = $this->findModel($id);
        if (!$task->isActive() || !$task->isRunning()) {
            return $this->goBack();
        }

        $task->stop();

        return $this->redirect(['/project/view', 'id' => $task->project->id]);
    }

    /**
     * @param integer $id
     * @return mixed
     */
    public function actionClose($id)
    {
        $task = $this->findModel($id);
        if (!$task->isActive()) {
            return $this->goBack();
        }

        $task->close();

        return $this->redirect(['/project/view', 'id' => $task->project->id]);
    }

    /**
     * Counts time for requested task, designed to work with ajax requests only.
     * @param integer id
     * @return JSON
     */
    public function actionGetTime($id)
    {
        $task = $this->findModel($id);

        if (!Yii::$app->request->isAjax) {
            Yii::warning('Action called not with ajax, but designed to be called only by ajax. See '
                .__FILE__.' at line '.__LINE__);
        }

        Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

        return [
            'taskId' => $id,
            'time' => $task->time,
        ];
    }
}
