<?php

namespace app\modules\api\controllers;

use \yii\rest\Controller;

class UserController extends Controller
{
    public function actionRegister()
    {
        return [__METHOD__];
    }

    public function actionLogin()
    {
        return [__METHOD__];
    }

    public function actionLogout()
    {
        return [__METHOD__];
    }

    public function actionDelete()
    {
        return [__METHOD__];
    }
}
