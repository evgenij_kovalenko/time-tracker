<?php

namespace app\controllers;

use app\modules\api\models\Project;
use Yii;
use yii\rest\ActiveController;
use yii\web\NotFoundHttpException;

/**
 * ProjectController implements the CRUD actions for Project model.
 */
class ProjectController extends ActiveController
{
    public $modelClass = 'app\models\Project';

//    protected function verbs()
//    {
//        return [
//            'index' => ['GET', 'HEAD'],
//            'view' => ['GET', 'HEAD'],
//            'create' => ['POST', 'PUT'],
//            'update' => ['PATCH'],
//            'delete' => ['DELETE'],
//        ];
//    }

    /**
     * Lists all Project models.
     * @return mixed
     */
//     public function actionIndex()
//     {
//         return Project::findAll([]);
//     }

    /**
     * @param $id
     *
     * @return Project
     * @throws NotFoundHttpException
     */
//    public function actionView($id)
//    {
//        return $this->findModel($id);
//    }

    /**
     * @return string|\yii\web\Response
     */
//    public function actionCreate()
//    {
//        $model = new Project();
//        $model->load(Yii::$app->request->post());
//        $model->save();
//    }

    /**
     * @param $id
     *
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException
     */
//    public function actionUpdate($id)
//    {
//        $model = $this->findModel($id);
//
//    }

    /**
     * @param $id
     *
     * @return array
     * @throws NotFoundHttpException
     * @throws \yii\db\StaleObjectException
     */
//    public function actionDelete($id)
//    {
//        $this->findModel($id)->delete();
//
//        return ['success' => true];
//    }

    /**
     * Finds the Project model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Project the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
//    protected function findModel($id)
//    {
//        if (($model = Project::findOne($id)) !== null) {
//            return $model;
//        } else {
//            throw new NotFoundHttpException('The requested project does not exist.');
//        }
//    }
}
