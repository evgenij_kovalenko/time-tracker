<?php

namespace app\models;

use yii\db\ActiveRecord;

/**
 * This is the model class for table "time_interval".
 *
 * @property integer $id
 * @property integer $task_id
 * @property string $start
 * @property string $end
 * @property integer $duration
 *
 * @property Task $idTask
 */
class TimeInterval extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->start = date('Y-m-d H:i:s');
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'time_interval';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['task_id', 'start', 'end'], 'integer'],
            [['task_id'], 'exist', 'skipOnError' => true, 'targetClass' => Task::className(), 'targetAttribute' => ['task_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'task_id' => 'Task Id',
            'start' => 'Start',
            'end' => 'End',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTask()
    {
        return $this->hasOne(Task::className(), ['id' => 'task_id']);
    }
}
