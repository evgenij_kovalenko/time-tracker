<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

use app\components\TimeFormatter;

/**
 * @property string $time
 */
class DailyReport extends Model
{
    public function fields()
    {
        return [
            'time' => function ($model) {

                $time = (new Query())->select(['time' => 'SUM(end-start)'])
                    ->from(TimeInterval::tableName())
                    ->where(['>', 'FROM_UNIXTIME(start)', date('Y-m-d')])->scalar();

                return TimeFormatter::format($time);
            },
        ];
    }
}
