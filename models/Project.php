<?php

namespace app\models;

use app\models\BaseModel;
use Yii;
use yii\db\Query;

use app\components\TimeFormatter;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 *
 * @property Task[] $tasks
 */
class Project extends BaseModel
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->status = true;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['project_id' => 'id']);
    }

    /**
     * Count
     * @return string
     */
    public function getTime()
    {
        $taskTable = Task::tableName();
        $timeTable = TimeInterval::tableName();
        $time = (new Query())->select(['time' => 'SUM(end-start)'])
            ->from($timeTable)
            ->leftJoin($taskTable, $timeTable.'.task_id='.$taskTable.'.id')
            ->where([
                $taskTable.'.project_id' => $this->id,
            ])->scalar();

        return TimeFormatter::format($time);
    }
}
