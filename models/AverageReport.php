<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;

use app\components\TimeFormatter;

use DateTime;

/**
 * @property string $time
 */
class AverageReport extends Model
{
    public function fields()
    {
        return [
            'time' => function ($model) {

                $allTime = (new Query())->select(['time' => 'SUM(end-start)'])
                    ->from(TimeInterval::tableName())->scalar();

                $first = (new Query())->select(['start'])
                    ->from(TimeInterval::tableName())
                    ->orderBy(['start' => SORT_ASC])
                    ->scalar();

                $days = (int)((time()-$first)/86400);

                $averageTime = (int)($allTime/$days);

                return TimeFormatter::format($averageTime);
            },
        ];
    }
}
