<?php

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

use DateTime;

/**
 * This is base model to work with create time.
 *
 * @property string $created_at
 */
abstract class BaseModel extends ActiveRecord
{
    public function init()
    {
        parent::init();

        $this->on(self::EVENT_BEFORE_INSERT, [$this, 'setCreateTime']);
    }

    public function setCreateTime()
    {
        $this->created_at = (new DateTime())->format('Y-m-d H:i:s');
    }
}
