<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\TimeInterval;

/**
 * TimeIntervalSearch represents the model behind the search form about `app\models\TimeInterval`.
 */
class TimeIntervalSearch extends TimeInterval
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'id_task', 'duration'], 'integer'],
            [['start', 'end'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = TimeInterval::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'id_task' => $this->id_task,
            'start' => $this->start,
            'end' => $this->end,
            'duration' => $this->duration,
        ]);

        return $dataProvider;
    }
}
