<?php

namespace app\models;

use app\models\BaseModel;
use Yii;
use yii\db\Query;

use app\components\TimeFormatter;

/**
 * This is the model class for table "task".
 *
 * @property integer $id
 * @property integer $project_id
 * @property string $name
 * @property string $description
 * @property string $created_at
 * @property string $updated_at
 * @property integer $status
 *
 * @property Project $idProject
 * @property TimeInterval[] $timeIntervals
 */
class Task extends BaseModel
{
    const STATUS_CLOSED = 0;
    const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        $this->status = self::STATUS_ACTIVE;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'status'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(),
                'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Project Id',
            'name' => 'Name',
            'description' => 'Description',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeIntervals()
    {
        return $this->hasMany(TimeInterval::className(), ['task_id' => 'id']);
    }

    /**
     * @return boolean
     */
    public function isActive()
    {
        return $this->status === self::STATUS_ACTIVE;
    }

    /**
     * @return boolean
     */
    public function isRunning()
    {
        return ($timeInterval = $this->getCurrentInterval()) !== null;
    }

    /**
     * @return mixed
     */
    public function getCurrentInterval()
    {
        return TimeInterval::findOne([
            'task_id' => $this->id,
            'end' => null,
        ]);
    }

    /**
     * @return void
     */
    public function run()
    {
        // Stop any running job on this task
        $this->stop();

        // And start new
        $timeInterval = new TimeInterval();
        $timeInterval->task_id = $this->id;
        $timeInterval->save();
    }

    /**
     * @return void
     */
    public function stop()
    {
        $update = Yii::$app->db->createCommand('UPDATE '.TimeInterval::tableName().' SET
            end = :now WHERE task_id = :task AND end IS NULL');
        $update->bindValue(':now', date('Y-m-d H:i:s'));
        $update->bindValue(':task', $this->id);

        $update->execute();
    }

    /**
     * @return void
     */
    public function close()
    {
        $this->stop();
        $this->status = self::STATUS_CLOSED;
        $this->save();
    }

    /**
     * Count
     * @return string
     */
    public function getTime()
    {
        $time = (new Query())->select(['time' => 'SUM(end-start)'])
            ->from(TimeInterval::tableName())
            ->where([
                'task_id' => $this->id
            ])->scalar();

        if ($this->isRunning()) {
            $interval = $this->getCurrentInterval();
            $time += time() - $interval->start;
        }

        return TimeFormatter::format($time);
    }

    /**
     * Creates list of statuses for building html select element
     * @return array
     */
    public static function getStatusList()
    {
        return [
            self::STATUS_CLOSED => 'Closed',
            self::STATUS_ACTIVE => 'Active',
        ];
    }
}
