<?php

namespace app\components;

class TimeFormatter
{
    /**
     * Formats time in seconds to hours/minutes
     * @param int $seconds
     * @return string
     */
    public static function format($seconds)
    {
        if ($seconds < 60) {
            return '';
        }

        $timeStr = [];
        $hours = (int)($seconds/3600);
        $secondsRest = $seconds;
        if ($hours > 0) {
            $secondsRest -= $hours*3600;
            $timeStr[] = $hours.'h';
        }

        $minutes = (int)($secondsRest/60);
        if ($minutes > 0) {
            $timeStr[] = $minutes.'m';
        }

        return implode(' ', $timeStr);
    }
}
