<?php

use yii\db\Migration;

class m200818_112100_CreateProjectRoleTable extends Migration
{
     // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('project_role', [
            'project_id' => $this->integer()->unsigned()->notNull(),
            'user_id' => $this->integer()->unsigned()->notNull(),
            'role_id' => $this->integer()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('pk_project_role', 'project_role', ['project_id', 'user_id']);

        $this->addForeignKey(
            'fk-project_role-project_id',
            'project_role',
            'project_id',
            'project',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-project_role-user_id',
            'project_role',
            'user_id',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-project_role-role_id',
            'project_role',
            'role_id',
            'role',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('fk-project_role-project_id', 'project_role');
        $this->dropForeignKey('fk-project_role-user_id', 'project_role');
        $this->dropForeignKey('fk-project_role-role_id', 'project_role');
        $this->dropTable('project_role');
    }
}
