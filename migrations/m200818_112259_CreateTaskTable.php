<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m200818_112259_CreateTaskTable extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey()->unsigned(),
            'project_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime(),
            'status' => $this->boolean()->notNull()->defaultValue(1),
            'created_by' => $this->integer()->unsigned()->notNull()->comment('user_id, who created task'),
        ]);

        $this->createIndex('idx-project-id', 'task', 'project_id');

        $this->addForeignKey(
            'fk-task-project_id',
            'task',
            'project_id',
            'project',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-task-created_by',
            'task',
            'created_by',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-task-project_id', 'task');
        $this->dropForeignKey('fk-task-created_by', 'task');
        $this->dropTable('task');
    }
}
