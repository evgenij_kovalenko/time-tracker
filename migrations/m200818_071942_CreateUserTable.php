<?php

use yii\db\Migration;

class m200818_071942_CreateUserTable extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('user', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
            'email' => $this->string()->notNull()->unique(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'status' => $this->boolean()->notNull()->defaultValue(1),
            'password' => $this->string()->notNull(),
            'salt' => $this->string()->notNull(),
        ]);
    }

    public function down()
    {
        $this->dropTable('user');
    }
}
