<?php

use yii\db\Migration;

class m200818_072353_CreateRoleTable extends Migration
{
     // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('role', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
            'description' => $this->string(),
            'status' => $this->boolean()->notNull()->defaultValue(1),
        ]);

        $this->insert('role', [
            'name' => 'owner',
            'description' => 'Who owns project; project: full access, can change project owner; task: full access; user: promote to owner, demote to user'
        ]);

        $this->insert('role', [
            'name' => 'admin',
            'description' => 'task: full access; user: promote to admin'
        ]);

        $this->insert('role', [
            'name' => 'user',
            'description' => 'owned task: full access; other task: change status, log work; user: no change'
        ]);
    }

    public function down()
    {
        $this->dropTable('role');
    }
}
