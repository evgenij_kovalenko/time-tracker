<?php

use yii\db\Migration;

/**
 * Handles the creation of table `project`.
 */
class m200818_112039_CreateProjectTable extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('project', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
            'description' => $this->text(),
            'created_at' => $this->dateTime()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->dateTime(),
            'status' => $this->boolean()->notNull()->defaultValue(1),
            'created_by' => $this->integer()->unsigned()->notNull()->comment('user_id, who created project'),
        ]);

        $this->addForeignKey(
            'fk-project-created_by',
            'project',
            'created_by',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-project-created_by', 'project');
        $this->dropTable('project');
    }
}
