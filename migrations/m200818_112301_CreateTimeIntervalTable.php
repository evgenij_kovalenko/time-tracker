<?php

use yii\db\Migration;

/**
 * Handles the creation of table `time_interval`.
 */
class m200818_112301_CreateTimeIntervalTable extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('time_interval', [
            'id' => $this->primaryKey()->unsigned(),
            'task_id' => $this->integer()->unsigned()->notNull(),
            'start' => $this->dateTime(),
            'end' => $this->dateTime(),
            'duration' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'created_by' => $this->integer()->unsigned()->notNull()->comment('user_id, who created entry'),
        ]);

        $this->createIndex('idx-task-id', 'time_interval', 'task_id');

        $this->addForeignKey(
            'fk-time_interval-task_id',
            'time_interval',
            'task_id',
            'task',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk-time_interval-created_by',
            'time_interval',
            'created_by',
            'user',
            'id',
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('fk-time_interval-task_id', 'time_interval');
        $this->dropForeignKey('fk-time_interval-created_by', 'time_interval');
        $this->dropTable('time_interval');
    }
}
